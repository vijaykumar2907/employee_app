﻿using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using Sample.Models;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Threading.Tasks;
using System.Xml.Linq;

namespace Sample.Controllers
{
    public class HomeController : Controller
    {
        XDocument xmldoc;
        private readonly ILogger<HomeController> _logger;

        public HomeController(ILogger<HomeController> logger)
        {
            _logger = logger;
        }
        public IActionResult Index()
        {
            xmldoc = XDocument.Load("Emp.xml");   //add xml document  
            var bind = xmldoc.Descendants("Employee").Select(p => new Employee
            {
                Id = p.Element("id").Value,
                Name = p.Element("name").Value,
                Salary = p.Element("salary").Value,
                Email = p.Element("email").Value,
                Address = p.Element("address").Value
            }).OrderBy(p => p.Id);

            List<Employee> emp = new List<Employee>();

            foreach(var item in bind)
            {
                emp.Add(item);
            }
           
            return View(emp);
        }

        [HttpGet]
        public IActionResult Create()
        {
            return View();
        }

        [HttpPost]
        public IActionResult Create(Employee emp)
        {
            xmldoc = XDocument.Load("Emp.xml");
            XElement emp1 = new XElement("Employee",
        new XElement("id", emp.Id),
        new XElement("name", emp.Name),
        new XElement("salary", emp.Salary),
        new XElement("email", emp.Email),
        new XElement("address", emp.Address));
            xmldoc.Root.Add(emp1);
            xmldoc.Save("Emp.xml");
            return RedirectToAction("Index");
        }

        public IActionResult Delete(string id)
        {
            xmldoc = XDocument.Load("Emp.xml");
            XElement emp = xmldoc.Descendants("Employee").FirstOrDefault(p => p.Element("id").Value == id);
            if (emp != null)
            {
                emp.Remove();
                xmldoc.Save("Emp.xml");
            }
            return RedirectToAction("Index");
        }
        public IActionResult Privacy()
        {
            return View();
        }

        [ResponseCache(Duration = 0, Location = ResponseCacheLocation.None, NoStore = true)]
        public IActionResult Error()
        {
            return View(new ErrorViewModel { RequestId = Activity.Current?.Id ?? HttpContext.TraceIdentifier });
        }
    }
}
