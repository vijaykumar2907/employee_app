﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Sample.Models
{
    public class Employee
    {
        public string Id { get; set; }

        public string Name { get; set; }

        public string Salary { get; set; }

        public string Email { get; set; }

        public string Address { get; set; }
    }
}
